class Product < ActiveRecord::Base
  self.per_page = 15
  belongs_to :category
  validates :name,        presence: true
  validates :category_id, presence: true
  validates :description, presence: true
  validates :size,        presence: true, length: { maximum: 100 }
  
  
end
