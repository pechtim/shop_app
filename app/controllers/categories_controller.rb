class CategoriesController < ApplicationController
  before_action :logged_in_user, only: [:index]
  before_action :admin_user, only: [:create, :destroy]
  
  
  #Создание новой категории
  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:success] = "New category added"
      redirect_to @category
    else
      render 'new'
    end
  end
  
  #Показ категории
  def show
    @category = Category.find(params[:id])
    @products = @category.products.paginate(page: params[:page])
    redirect_to category_products_path(params[:id])
  end
  
  def new
    if !current_user.admin?
      redirect_to(categories_url)
    else
      @category = Category.new
    end
  end
  
  def index
    @categories = Category.paginate(page: params[:page])
  end
  
  def destroy
    Category.find(params[:id]).destroy
    flash[:success] = "Category and all the products within it were deleted"
    redirect_to categories_url
  end
  
    private
    
      def category_params
        params.require(:category).permit(:name)
      end
      
      def admin_user
        redirect_to(root_url) unless current_user.admin?
      end
end
