class ProductsController < ApplicationController
  before_action :logged_in_user, only: [:index]
  before_action :admin_user, only: [:create, :destroy]
  
  def new
    if !current_user.admin?
      redirect_to(categories_url)
    else
      @product = Product.new
    end
  end
  
  def show
    @product = Product.find(params[:id])
  end
  
  def index
    @category = Category.find(params[:category_id])
    @products = @category.products.paginate(page: params[:page])
  end
  
  def create
    @product = current_category.products.build(product_params)
    if @product.save
      flash[:success] = "Product added!"
      redirect_to category_products_url(@current_category)
    end
  end
  
  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    flash[:success] = "Product deleted"
    redirect_to request.referrer || categories_url(@current_category)
  end
  
  private
    def product_params
      params.require(:product).permit(:name, :size, :description)
    end
    
    def admin_user
        redirect_to(root_url) unless current_user.admin?
    end
end
