module CategoriesHelper
  
  def current_category
    @current_category ||= Category.find_by(id: params[:category_id])
  end
  
end
