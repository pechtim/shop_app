ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Возвращает true, если тестовый пользователь осуществил вход.
  def is_logged_in?
    !session[:user_id].nil?
  end
  
  def log_in_as(user, options = {})
    password    = options[:password]    || 'password'
    remember_me = options[:remember_me] || '1'
    if integration_test?
      post login_path, params: { session: { email:       user.email,
                                           password:    password,
                                           remember_me: remember_me } }
    else
      session[:user_id] = user.id
    end
  end

  private

    # Возвращает true внутри интеграционных тестов
    def integration_test?
      defined?(post follow_redirect!) #post_via_redirect deprecated. Подстановка старых значений как в учебнике выдаст 2 ошибки в 2 тестах,  
    end                               #где используется этот метод.
end
