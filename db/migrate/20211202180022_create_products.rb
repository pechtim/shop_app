class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.references :category, foreign_key: true
      t.string :name
      t.string :size
      t.text   :description
      
      t.timestamps
    end
  end
end
